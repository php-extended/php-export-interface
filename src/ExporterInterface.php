<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-export-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Export;

use Iterator;
use Stringable;

/**
 * ExporterInterface interface file.
 * 
 * This interface specifies a blob method for exporting data into a standardized
 * form. The specifics of this format vary but the return data of both methods
 * can directly be used to be written on files.
 * 
 * @author Anastaszor
 */
interface ExporterInterface extends Stringable
{
	
	/**
	 * Exports an integer value.
	 * 
	 * @param ?integer $value
	 * @return integer the number of bytes written
	 */
	public function exportInteger(?int $value) : int;
	
	/**
	 * Exports a float value.
	 * 
	 * @param ?float $value
	 * @return integer the number of bytes written
	 */
	public function exportFloat(?float $value) : int;
	
	/**
	 * Exports a string value.
	 * 
	 * @param ?string $value
	 * @return integer the number of bytes written
	 */
	public function exportString(?string $value) : int;
	
	/**
	 * Exports a single object.
	 * 
	 * @param ?object $value
	 * @return integer the number of bytes written
	 */
	public function exportObject($value = null) : int;
	
	/**
	 * Exports an array of objects.
	 * 
	 * @param array<integer, object> $values
	 * @return integer the number of bytes written
	 */
	public function exportArray(array $values) : int;
	
	/**
	 * Exports a list of objects.
	 *
	 * @param Iterator<object> $iterator
	 * @return integer the number of bytes written
	 */
	public function exportIterator(Iterator $iterator) : int;
	
}
